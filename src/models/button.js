import { ObjectModel } from "objectmodel"

const Button = new ObjectModel({
	title: String,
})

export const ButtonWithPageSlug = Button.extend({
	pageSlug: String,
}).as("ButtonWithPageSlug")

export const ButtonWithUrl = Button.extend({
	url: String,
}).as("ButtonWithUrl")
